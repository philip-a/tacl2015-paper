.PHONY: all cover decision dvi pdf runpdf clean

TEX = platex --kanji=utf8
BIBTEX = pbibtex --kanji=utf8
DVIPDF = dvipdfmx
PDFLATEX = pdflatex
PREVIEW = open -a preview

COVER = cover-letter.tex
DECISION = decision.tex 
RESUBMISSION = resubmission-pkg.tex 
BIB = tacl2015.bib
SRC = tacl2015.tex
MAIN= $(SRC:.tex=)
DVI = $(SRC:.tex=.dvi)
PDF = $(SRC:.tex=.pdf)

all: cover decision pdf
	$(PDFLATEX) $(RESUBMISSION)

cover:
	$(PDFLATEX) $(COVER)

decision:
	$(PDFLATEX) $(DECISION)

dvi:
	$(TEX) $(MAIN)
	$(BIBTEX) $(MAIN)
	$(TEX) $(MAIN)	
	$(TEX) $(MAIN)

pdf: dvi
	$(DVIPDF) $(DVI)

runpdf: pdf
	$(PREVIEW) $(PDF)

clean:
	rm -f *.bbl *.blg *.aux *.log *.dvi *.pdf
